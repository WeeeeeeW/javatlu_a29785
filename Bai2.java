


class Node{
    int item;
    Node next;
    Node(){
        item = 0;
        next = null;
    }
    Node(int i){
        item = i;
        next = null;
    }
}

class Stack{
    Node top;
    int size;
    Stack(){
        top = null;
        size = 0;
    }


    public void push(int i) {
        if(size == 0)
            top = new Node(i);
        else{
            Node temp = new Node(i);
            temp.next = top;
            top = temp;
        }    
        size++;
    }


    public int pop(){
        if(size > 0)
        {
            Node temp;
            temp = top;
            top = top.next;
            size--;
            return temp.item;
        }
        else
            return -1;
    }


    public boolean isEmpty(){
        if(size > 0 )
        {
            System.out.println("Stack is not empty");
            return false;
        }
        System.out.println("Stack is empty");
        return true;
    }

    public int numOfElement(){
        System.out.println("Stack size: "+size);
        return size;
    }

    public int search(int k){
        int index = 0;
        Node cur = top;
        while (cur != null) {
            index++;
            if(cur.item == k){

                System.out.println("Index: "+index);
                return index;
            }
            cur = cur.next;
        }
        return 0;
    }

    public void display(){
        Node temp = top;
        while(temp != null)
        {
            System.out.print(temp.item + " ");
            temp = temp.next;
        }
        System.out.println("");
    }
}

public class Bai2{
    public static void main(String[] args){
        Stack a = new Stack();
        a.isEmpty();
        a.push(2);
        a.push(1);
        a.display();
        a.pop();
        a.display();
        a.numOfElement();
        a.isEmpty();
        a.push(4);
        a.push(3);
        a.search(2);
        a.display();
    }
}
