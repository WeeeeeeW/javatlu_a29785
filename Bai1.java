public class Bai1
{
    public static void main(String[] args)
    {
        Employee A = new Employee();
        Employee B = new Employee();
        A.setSalary(100);
        B.setSalary(-5);
        System.out.println("A's salary = " + A.getSalary()*12);
        System.out.println("B's salary = " + B.getSalary()*12);
        A.setSalary(A.getSalary()*1.1);
        B.setSalary(B.getSalary()*1.1);
        System.out.println("A's salary = " + A.getSalary()*12);
        System.out.println("B's salary = " + B.getSalary()*12);
    }
}
class Employee
{
    private String Firstname;
    private String Lastname;
    private Double Salary;
    public void setFirstname(String Fname){
        Firstname = Fname;
    }
    public String getFirstname(){
        return Firstname;
    }
    public void setLastname(String Lname){
        Firstname = Lname;
    }
    public String getLastname(){
        return Lastname;
    }
    public void setSalary(double salary){
        if(salary < 0){
            salary = 0.0;
        }
        Salary = salary;
    }
    public double getSalary(){
        return Salary;
    }
}